<div class="body">
<h2>Literatur</h2>

<p>Literaturangaben</p>
<p><em> Warum Literaturangaben?</em> Literaturangaben sind immer dann notwendig, wenn man ein wörtliches Zitat bzw. einen
direkten Verweis auf einen Dritten in seinem Text benutzt. Zitate müssen im eigenen Text mit Anführungszeichen
gekennzeichnet werden. </p>

<p><em>In welchem Maß muss bzw. kann Literatur angegeben werden?</em>
Grundsätzlich müssen Zitate und direkte Verweise im Text mit einer Referenz
auf das entsprechende Werk und die entsprechende Stelle (meist in Form einer
Seitenzahl) versehen werden. Alle referenzierten Werke werden im
Literaturverzeichnis gesammelt, wo sie mit allen Angaben (Autor, Titel,
Erscheinungsjahr, Verlag, etc.) aufgeführt werden. Oftmals ist es sinnvoll,
die verwendete, aber nicht direkt zitierte Literatur ebenfalls im
Literaturverzeichnis anzugeben. Dies erleichtert dem Leser, sich eingehend
mit den verfassten Texte auseinanderzusetzen, und entspricht guter
wissenschaftlicher Praxis, ist aber optional.
</p>

<h4><a name="littext">Eingabe der Literaturangaben im Text </a></h4>
<p> Literaturangaben und Zitatnachweise im Text werden mithilfe eine Kurzform gesetzt,
 die Dokuforge signalisiert, welches Buch hier referenziert. Natürlich muss das so zitierte Buch unter
dieser Kurzform in der Bibliographie (= Literaturverzeichnis) aufgeführt werden, damit es eindeutig zugeordnet
werden kann. Nach der Kurzform wird alles angegeben, was die zitierte Stelle näher spezifiziert, also meist eine Seitenzahl.

Ein Verweis auf Seite 112 in ein Werk mit dem Kurzform "fischer97" sieht
folgendermaßen aus (wichtig ist dabei die Auszeichnung "LITERATUR:" die
verwendet wird um eine Referenz zu kennzeichnen).</p>

<p style="margin-left: 3em"> "Dies ist ein Zitat eines Werkes."
(LITERATUR:fischer97 112) </p>

<p>Alle so zitierten Werke müssen auf jeden Fall mit vollständigen Angaben
(siehe Auflistung unten) an das Ende des jeweiligen Teils gestellt
werden. Wir werden später je Kurs ein Gesamtverzeichnis erstellen, das am
Schluss der entsprechenden Kursdoku stehen wird.  Die Angabe zu obigem Zitat
könnte etwa wie folgt aussehen (beachte die Kurzform "fischer97" am
Anfang).  </p>

<pre style="margin-left: 3em">
@BOOK{fischer97,
  title		= "Lineare Algebra",
  author	= "Gerd Fischer",
  year		= "1997",
  address	= "Berlin",
  publisher	= "Vieweg Studium"
}
</pre>

<h4><a name="litsatz">Eingabe der Literaturdaten </a></h4>
<p> Die Angaben für das Literaturverzeichnis werden in eine Ednote gesetzt, wobei dafür idealerweise eine Seite am Ende des
jeweiligen Kurses angelegt wird. Dort wird dann die gesamte Literatur des Kurses gesammelt. Im Literaturverzeichnis
kann die Literatur auch wahlweise getrennt pro Kurskapitel angegeben werden. Das Literaturverzeichnis baut
ihr in der folgenden Form in eine Ednote ein: </p>
<ul><p><pre>
{({ LITERATUR

 Hier stehen die Literaturangaben...

})}
</pre></p></ul>
<!-- Die Literatureingabe erfolgt direkt im (zugegebenermaßen) etwas kryptischen BibLaTeX-Stil.
(dieser sollte aber soweit verständlich sein).-->
<p>
Die entsprechenden Literaturangaben werden folgendermaßen umgesetzt, hier am Beispiel von verschiedenen häufig vorkommenden
Publikationsformen. (Für die Erfahrenen sei gesagt, dass zum Satz BibLaTeX verwendet wird.) <!--Für viele Referenzen
gibt es bereits vorgefertigte Vorlagen, die man natürlich verwenden kann.--> Die Angabe in der ersten Zeile ist die eindeutige
Kurzform, die für Verweise im Text verwendet wird.</p>

<ul><li><p> Im Fall einer Monographie/Buch lautet die Syntax:</p>
<p><pre>
@BOOK{fischer97,							"Kurzform"
  title		= "Lineare Algebra",					"Vollständiger Titel der Monographie"
  author	= "Gerd Fischer",					"Name der Autoren"
  editor        = "Heinz Rühmann",      %% optional %%                  "Editor des Werkes"
  translator    = "Wilhelm Braun",      %% optional %%                  "Übersetzer des Werkes"
  year		= "1997",						"Veröffentlichungsjahr"
  address	= "Berlin"						""Ort der Veröffentlichung"
  publisher	= "Vieweg Studium",    %% optional %%			"Verlag"
}
</pre></p></li></ul>

<ul><li><p> Im Fall eines Artikels in einer Zeitschrift/Journal lautet die Syntax (bei Zeitungsartikeln bitte das
vollständige Datum angeben):</p>
<p><pre>
@ARTICLE{heisenbergDeutung,						"Kurzform"
  author	= "Werner Heisenberg",					"Name der Autoren"
  title		= "Die Kopenhagener Deutung der Quantentheorie",	"Vollständiger Titel des Artikels"
  journal	= "Physik und Philosophie",				"Name des Journals, in dem der Artikel veröffentlicht wurde"
  volume	= "12",							"Nummer des Jahrgangs"
  year		= "1990",						"Veröffentlichungsjahr"
  pages		= "27--42",						"Seitenumfang im Journal"
}
</pre></p></li></ul>

<ul><li><p> Im Fall eines Aufsatzes in einem Sammelwerk (hierunter fallen auch Artikel in Lexika)
lautet die Syntax:</p>
<p><pre>
@INCOLLECTION{brodersen,						"Kurzform"
  author	= "Brodersen, Kai",					"Name der Autoren"
  title		= "Gebet und Fluch, Zeichen und Traum.",		"Titel des Aufsatzes"
  editor	= "Brodersen, Kai",					"Name der Herausgeber"
  booktitle	= "Briefe in die Unterwelt.",				"Name des Sammelwerkes/Lexikons"
  address	= "Münster",						"Ort der Veröffentlichung"
  year		= "2001",						"Jahr der Veröffentlichung"
  pages		= "57--68",						"Seitenumfang im Sammelwerk"
}
</pre></p></li></ul>

<ul><li><p> Im Fall einer URL bzw. Internetreferenz lautet die Syntax:</p>
<p><pre>
@BOOKLET{mpgBericht,							"Kurzform"
  url 		= "http://www.mpg.de/180882/Jahresbericht",		"Internetadresse"
  urldate	= "02.07.2012",						"Datum des Abrufes"
  title		= "Jahresbericht der Max-Planck-Gesellschaft 2010",	"Titel der Internetreferenz"
  author	= "Max-Planck-Gesellschaft,	%% optional %%		"Name des Autors
}
</pre></p></li></ul>

<p> Die obigen Beispiele dienen zur Illustration der Verwendung der Literaturangaben. In der Kürze ist es natürlich
nicht möglich, alle Spezialfälle abzudecken. Wenn ihr eine Literaturangabe habt, die ihr in keines der oben genannten
Schemata packen könnt, dann schreibt die fertig formatierte Literaturangabe in eine eigene Ednote. Um die endgültige
Formatierung kümmern wir uns dann in der Redaktion.</p>
</div>
